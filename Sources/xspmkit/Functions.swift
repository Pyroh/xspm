//
//  Functions.swift
//  xspmkit
//

import Foundation
import PathKit

public func cleanFiles(in root: Path, keepManifest flag: Bool = true) throws {
    LoggerManager.logDebug("Reading manifest...")
    let manifest = try Manifest.read(inRoot: root)
    if (flag) {
        LoggerManager.logDebug("Sparing manifest's life.")
    } else {
        LoggerManager.logDebug("Deleting manifest file.")
        let manifestPath = root + Manifest.manifestFileName
        try manifestPath.delete()
    }
    
    LoggerManager.logDebug("Checking if resolved manifest exists")
    let resolvedPath = root + Manifest.resolvedFileName
    if resolvedPath.exists {
        LoggerManager.logDebug("Deleting resolved file.")
        try resolvedPath.delete()
    }
    
    LoggerManager.logDebug("Deleting dependencies folder.")
    guard manifest.dependencyFolderPath.exists else {
        LoggerManager.logDebug("Nope it doesn't exist.")
        return
    }
    try manifest.dependencyFolderPath.delete()
    LoggerManager.logDebug("Done.")
}
