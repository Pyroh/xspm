//
//  ComparisonResult.swift
//  xspmkit
//

import Foundation

public struct ManifestComparisonResult: OptionSet {
    public let rawValue: Int
    
    public var requirePackageRewrite: Bool {
        return self.contains(.dependencies) || self.contains(.dependenciesName) || self.contains(.toolsVersion)
    }
    
    public init(rawValue: Int) { self.rawValue = rawValue }
    
    /// Indicates that the project name has been changed.
    public static let projectName = ManifestComparisonResult(rawValue: 1 << 0)
    
    /// Indicates that the dependencies folder name has been changed.
    public static let dependenciesName = ManifestComparisonResult(rawValue: 1 << 1)
    
    /// Indicates that the dependencies list has been changed.
    public static let dependencies = ManifestComparisonResult(rawValue: 1 << 2)
    
    /// Indicates that the dependencies mapping has been changed.
    public static let dependenciesMapping = ManifestComparisonResult(rawValue: 1 << 3)
    
    /// Reserved
    public static let actualDependenciesMapping = ManifestComparisonResult(rawValue: 1 << 4)
    
    /// Indicates that the embed dependencies flag has been changed.
    public static let embedDependencies = ManifestComparisonResult(rawValue: 1 << 5)
    
    /// Indicates that the link dependencies flag has been changed.
    public static let linkDependencies = ManifestComparisonResult(rawValue: 1 << 6)
    
    /// Indicates that the tools version has been changed.
    public static let toolsVersion = ManifestComparisonResult(rawValue: 1 << 7)
    
    public static let xcconfig = ManifestComparisonResult(rawValue: 1 << 8)
}
