//
//  Dependency.swift
//  xspmkit
//

import Foundation

public struct Dependency: Codable {
    let products: [String]
    let url: String
    let version: Version
    
    init(products: [String], url: String, version: Version) {
        let wurl = url as NSString
        self.url = wurl.isAbsolutePath ? wurl.standardizingPath : url
        self.products = products
        self.version = version
    }
}

extension Dependency: CustomStringConvertible {
    public var description: String {
        return "\(self.url)\n\t- version: \(self.version)\n\t- products: \(self.products.joined(separator: ", "))"
    }
}

extension Dependency: SPMManifestSerializable {
    var serialized: String {
        return ".package(url: \(self.url.quoted), \(self.version.serialized))"
    }
}

extension Dependency: Equatable {
    public static func == (lhs: Dependency, rhs: Dependency) -> Bool {
        // It is sufficient at the time this code is written. 
        return lhs.url == rhs.url && lhs.version == rhs.version && lhs.products == rhs.products
    }
}
