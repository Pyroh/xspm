//
//  Tokens.swift
//  xspmkit
//

import Foundation

enum Token {
    case none
    case kind(TokenData)
    case identifier(TokenData)
    case string(TokenData)
    case number(TokenData, Bool)
    case bool(TokenData, Bool)
    case semver(TokenData)
    case revision(TokenData)
    case comma(TokenData)
    case openbracket(TokenData)
    case closebracket(TokenData)
    case errorneous(TokenData)
    case op(TokenData)
    case colon(TokenData)
    case whiteSpace
    case eol
    case eof
    
    var data: TokenData? {
        switch self {
        case .kind(let data), .identifier(let data), .string(let data), .number(let data, _), .bool(let data, _), .semver(let data), .revision(let data), .comma(let data), .openbracket(let data), .closebracket(let data), .errorneous(let data), .op(let data), .colon(let data):
            return data
        default:
            return nil
        }
    }
    
    var isSpecialDelimiter: Bool {
        switch self {
        case .comma(_):
            return true
        default:
            return false
        }
    }
}

