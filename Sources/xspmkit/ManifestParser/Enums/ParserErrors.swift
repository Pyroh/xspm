//
//  ParserErrors.swift
//  xspmkit
//

import Foundation

public enum ParserError: Error {
    public typealias Position = (col: Int, line: Int, pos: Int)
    
    case unexpectedToken(position: Position, text: String)
    case expected(position: Position, type: String)
    case syntaxError(position: Position, reason: String)
    case missingProjectName
    case duplicateProperty(name: String)
    case duplicateMapping(name: String)
    case unknown
    
    func logError() {
        switch self {
        case .unexpectedToken(let position, let text):
            LoggerManager.logError("Manifest syntax error\n\t\(position.line):\(position.col) : “\(text)“ unexpected token.")
        case .expected(let position, let type):
            LoggerManager.logError("Manifest syntax error\n\t\(position.line):\(position.col) : expected \(type).")
        case .syntaxError(let position, let reason):
            LoggerManager.logError("Manifest syntax error\n\t\(position.line):\(position.col) : syntax error \(reason).")
        case .missingProjectName:
            LoggerManager.logError("Manifest: missing project name.")
        case .duplicateProperty(let propertyName):
            LoggerManager.logError("Manifest: duplicate property: \(propertyName)")
        case .duplicateMapping(let mappingName):
            LoggerManager.logError("Manifest: duplicate dependency mapping: \(mappingName)")
        case .unknown:
            LoggerManager.logError("Manifest: unkonw error, but it's serious. For sure.")
        }
    }
}
