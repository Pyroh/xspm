//
//  TokenData.swift
//  xspmkit
//

struct TokenData {
    let start: Streamer.Location
    
    private(set) var end: Streamer.Location
    private(set) var buffer: String
    
    private(set) var isClosed: Bool
    
    init(start: Streamer.Location) {
        self.start = start
        self.end = .undefined
        self.buffer = ""
        self.isClosed = false
    }
    
    mutating func append(_ char: Character) {
        guard !self.isClosed else { fatalError("You can't modify closed token data !") }
        self.buffer.append(char)
    }
    
    mutating func close(at location: Streamer.Location) {
        guard self.end == .undefined else { fatalError("You can't close token data twice !") }
        self.end = location
        self.isClosed.toggle()
    }
    
    static func initial() -> TokenData {
        var td = TokenData.init(start: .undefined)
        td.isClosed = true
        
        return td
    }
}
