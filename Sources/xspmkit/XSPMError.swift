//
//  XSPMError.swift
//  xspmkit
//

import Foundation

public enum XSPMError: Error {
    // Bootstrap
    case manifestAlreadyExists
    case noSuchProject(String)
    case projectNameIndecidable
    case noProject
    
    // Init
    case invalidDependenciesFolderName
    case alreadyInitiated
    case invalidManifest
    case noManifest
    case cannotCreatePackage
    case noDependencies
    case swiftPMError(String)
    case ambiguousState
    
    // Update
    case cannotReadResolved
    
    // Common
    case unknown(String)
    case cannotGetDependenciesTree
    case unresolvedManifest
    case cannotDecribePackage
}
