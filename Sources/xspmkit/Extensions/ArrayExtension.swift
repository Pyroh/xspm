//
//  ArrayExtension.swift
//  xspmkit
//

import Foundation

extension Array where Element: Hashable {
    var set: Set<Element> { return Set<Element>(self) }
    var unique: [Element] { return self.set.array }
}
