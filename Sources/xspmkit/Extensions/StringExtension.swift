//
//  StringExtension.swift
//  xspmkit
//
import Foundation

extension String {
    var quoted: String {
        return "\"\(self)\""
    }
    
    mutating func appendLine(_ line: String, indentLevel: Int = 0) {
        let indent = String(repeating: "\t", count: indentLevel)
        self += "\n\(indent)\(line)"
    }
    
    mutating func lineFeed() {
        self += "\n"
    }
    
    func removingFirstAndLast() -> String {
        var str = self
        str.removeFirstAndLast()
        return str
    }
    
    mutating func removeFirstAndLast() {
        self.removeFirst()
        self.removeLast()
    }
    
    public init?(utf8Data data: Data) {
        self.init(data: data, encoding: .utf8)
    }
}
