//
//  BootstrapCommand.swift
//  xspm
//

import Foundation
import Commandant
import Curry
import Result
import PathKit
import xspmkit

fileprivate let defaultProjectName: String = "./AUTO/."

struct BootstrapCommandOptions: OptionsProtocol, VerboseOptionsProtocol, QuietOptionsProtocol {
    typealias ClientError = XSPMError
    
    let projectName: String
    let forceOverwrite: Bool
    let ommitExample: Bool
    let verbose: Bool
    let quiet: Bool
    
    static func evaluate(_ m: CommandMode) -> Result<BootstrapCommandOptions, CommandantError<XSPMError>> {
        return curry(self.init)
            <*> m <| Argument(defaultValue: defaultProjectName, usage: "Name of the target project", usageParameter: "name")
            <*> m <| Switch(flag: "f", key: "force", usage: "Overwrite existing manifest file")
            <*> m <| Switch(flag: "i", key: "no-example", usage: "Don't add example to the manifest file")
            <*> m <| verboseSwitch
            <*> m <| quietSwitch
    }
}

struct BootstrapCommand: XSPMCommandProtocol {
    typealias Options = BootstrapCommandOptions
    
    var verb: String = "bootstrap"
    var function: String = "🏁 - Create the manifest file"
    
    func run(_ options: BootstrapCommandOptions) throws {
        let root = Path.current
        let projectName: String
        
        CommonOutput.say(that: "\n🏁 Bootstraping.\n".bold)
        CommonOutput.inform(that: "Creating \(Manifest.manifestFileName) file...")
        
        if options.projectName == defaultProjectName {
            let projects = root.glob("*.xcodeproj").map { $0.lastComponent }
            
            guard !projects.isEmpty else { throw XSPMError.noProject }
            guard projects.count == 1 else { throw XSPMError.projectNameIndecidable }
            
            projectName = projects[0]
        } else {
            let projectPath = root + options.projectName
            
            guard projectPath.exists else {
                throw XSPMError.noSuchProject(options.projectName)
            }
            
            projectName = options.projectName
        }
        
        try Manifest.create(inRoot: root, forProject: projectName, forceOverwrite: options.forceOverwrite, ommitExamples: options.ommitExample)
        
        CommonOutput.sayItsOkay(because: "\(Manifest.manifestFileName) file created.")
        CommonOutput.sayJobsDone()
    }
}
