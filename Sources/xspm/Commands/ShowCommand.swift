//
//  File.swift
//  xspm
//
//

import Foundation

import Foundation
import Commandant
import Curry
import Result
import PathKit
import xspmkit

struct ShowCommandOptions: OptionsProtocol, VerboseOptionsProtocol {
    typealias ClientError = XSPMError
    
    let verbose: Bool
    
    static func evaluate(_ m: CommandMode) -> Result<ShowCommandOptions, CommandantError<XSPMError>> {
        return curry(self.init)
            <*> m <| verboseSwitch
    }
}

struct ShowCommand: XSPMCommandProtocol {
    typealias Options = ShowCommandOptions
    
    var verb: String = "show"
    var function: String = "🌳 - Show resolved dependency tree"
    
    func run(_ options: ShowCommandOptions) throws {
        let root = Path.current
        
        CommonOutput.say(that: "\n🌳 Showing dependencies tree...\n".bold)
        let manifest = try Manifest.read(inRoot: root)
        
        try manifest.assert(is: .ok)
        let dependenciesTree = try manifest.getSPMDependenciesTree()
        let dependenciesMappings = try manifest.getResolvedDependenciesMapping()
        
        CommonOutput.sayItsOkay(because: "Dependency tree:")
        CommonOutput.say(that: dependenciesTree.description)
        
        CommonOutput.feed()
        
        CommonOutput.sayItsOkay(because: "Per-target dependency mapping:")
        dependenciesMappings.forEach {
            CommonOutput.say(that: $0.description)
        }
        
        CommonOutput.sayJobsDone()
    }
}
