//
//  SaveLogsOptionsProtocol.swift
//  xspm
//

import Foundation

import Foundation
import Commandant

protocol SaveLogsOptionsProtocol {
    var saveLogs: Bool { get }
    static var saveLogsSwitch: Switch { get }
}

extension QuietOptionsProtocol {
    static var saveLogsSwitch: Switch {
        return Switch(flag: "l", key: "save-logs", usage: "Save all spm logs")
    }
}
