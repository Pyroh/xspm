APP_NAME = xspm

ROOT = /usr/local
INSTALL_PATH = $(ROOT)/bin/$(APP_NAME)
BUILD_PATH = .build/release/$(APP_NAME)

.PHONY: build

install: build
	install -d "$(ROOT)/bin"
	install -C -m 755 $(BUILD_PATH) $(INSTALL_PATH)

build:
	swift build --disable-sandbox -c release --static-swift-stdlib

uninstall:
	rm -f $(INSTALL_PATH)

dev:
	swift package generate-xcodeproj
