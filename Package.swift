// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "xspm",
    platforms: [
        .macOS(.v10_13)
    ],
    products: [
        .executable(name: "xspm", targets: ["xspm", "xspmkit"]),
        .library(name: "xspmkit", targets: ["xspmkit"]),
    ],
    dependencies: [
        .package(url: "https://github.com/tuist/xcodeproj.git", .upToNextMajor(from: "6.0.0")),
        .package(url: "https://github.com/onevcat/Rainbow", from: "3.0.0"),
        .package(url: "https://github.com/Pyroh/Commandant.git", .branch("master")),
        .package(url: "https://github.com/Pyroh/Curry", .branch("master")),
        .package(url: "https://github.com/antitypical/Result.git", from: "4.0.0"),
        .package(url: "https://github.com/Pyroh/ConvenientCharacterSets.git", from: "1.0.0"),
        .package(url: "https://github.com/kylef/PathKit", .upToNextMinor(from: "0.9.2"))
    ],
    targets: [
        .target(
            name: "xspm",
            dependencies: ["Rainbow", "Commandant", "Curry", "PathKit", "xspmkit"]),
        .target(
            name: "xspmkit",
            dependencies: ["xcodeproj", "ConvenientCharacterSets", "Result", "PathKit"]),
        .testTarget(
            name: "xspmkitTests",
            dependencies: ["xspmkit"]),
    ]
)

